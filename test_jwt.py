import base64
import os
import time
from jose import jwk, jwt

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.hazmat.primitives.serialization import load_pem_private_key, load_pem_public_key


def generate_rsa_private_key(key_size=2048):

    return rsa.generate_private_key(public_exponent=65537,
                                    key_size=key_size,
                                    backend=default_backend())


def get_private_key_in_pem_fmt(private_key):
    return private_key.private_bytes(encoding=serialization.Encoding.PEM,
                                     format=serialization.PrivateFormat.TraditionalOpenSSL,
                                     encryption_algorithm=serialization.NoEncryption())

def get_public_key_in_pem_fmt(public_key):
    return public_key.public_bytes(encoding=serialization.Encoding.PEM,
                                   format=serialization.PublicFormat.SubjectPublicKeyInfo)

def load_private_key(filename):
    with open(filename, 'rb') as pem_in:
        pemlines = pem_in.read()
    private_key = load_pem_private_key(pemlines, None, default_backend())
    return private_key

def load_public_key(filename):
    with open(filename, 'rb') as pem_in:
        pemlines = pem_in.read()
    private_key = load_pem_public_key(pemlines, default_backend())
    return private_key


if __name__ == '__main__':
    #import pdb; pdb.set_trace()
    if 0:
        private_key = generate_rsa_private_key()
        public_key = private_key.public_key()
        private_key_in_pem_fmt = get_private_key_in_pem_fmt(private_key)
        public_key_in_pem_fmt = get_public_key_in_pem_fmt(public_key)

        with open('rsa_key', 'wb') as f:
            f.write(private_key_in_pem_fmt)

        with open('rsa_key.pub', 'wb') as f:
            f.write(public_key_in_pem_fmt)

    private_key = load_private_key('data/rsa_key')
    public_key = load_public_key('data/rsa_key.pub')
    private_key_in_pem_fmt = get_private_key_in_pem_fmt(private_key)
    public_key_in_pem_fmt = get_public_key_in_pem_fmt(public_key)

    #jwk_id = base64.b32encode(os.urandom(10)).lower().decode('utf-8')
    jwk_id = 'dbvzao6l5evcnspa'
    headers = {'kid': jwk_id,
               'jku': 'http://localhost/jwks'}

    token_claims = {'iss': 'authgate',
                    #'iat': int(time.time()),
                    #'exp': int(time.time()) + 600,
                    'iat': 1555930834,
                    'exp': 1558522834,
                    'ten': 'ibm',
                    'sub': 'user1'}

    #import pdb; pdb.set_trace()

    token = jwt.encode(token_claims,
                       private_key_in_pem_fmt,
                       algorithm=jwt.ALGORITHMS.RS256,
                       headers=headers)
    print("token:", token)
    print("unverified hdr:", jwt.get_unverified_header(token))
    import sys
    sys.exit(0)

    # generate JWK based on public key
    jwk_object = jwk.construct(public_key, jwk.ALGORITHMS.RS256)
    jwk_dict = jwk_object.to_dict()
    jwk_dict['kid'] = jwk_id

    claims = jwt.decode(token, jwk_dict, algorithms=jwt.ALGORITHMS.RS256)
    print("jwk_dict:", jwk_dict)
    print("claims:", claims)
